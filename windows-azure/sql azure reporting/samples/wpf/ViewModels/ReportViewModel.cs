﻿using System.Configuration;

namespace Syncfusion.Samples.ViewModel
{
    public class ReportViewModel 
    {
        public ReportModel Report { get; set; }
           
        #region Constructor

        public ReportViewModel()
        {
            this.Report = new ReportModel();
            this.Report.ReportPath = ConfigurationManager.AppSettings["reportFileName"];
            // SQL Azure Report Server URL 
            this.Report.ReportServerUrl = ConfigurationManager.AppSettings["reportServerUrl"];
        }
        #endregion
    }
}
