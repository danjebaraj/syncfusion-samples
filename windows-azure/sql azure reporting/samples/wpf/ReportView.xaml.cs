﻿#region Copyright Syncfusion Inc. 2001 - 2011
// Copyright Syncfusion Inc. 2001 - 2011. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion

using System.Collections.Generic;
using System.Windows;
using System.Configuration;
using Syncfusion.Windows.Reports;

namespace Syncfusion.Samples
{
    public partial class ReportView : Window
    {
        #region Constructor

        public ReportView()
        {
           
            InitializeComponent();

            
            this.Loaded += (o,e) =>
                               {
                                   viewer.ReportServerFormsCredential = new Syncfusion.Windows.Reports.ReportServerFormsCredential(ConfigurationManager.AppSettings["user"],ConfigurationManager.AppSettings["password"]);
                                   viewer.RefreshReport();
                               };

            this.viewer.ReportLoaded += (o, e) =>
            {
                var dataSources = this.viewer.GetDataSources();
                var info = new List<DataSourceCredentials>();

                foreach (var dataSource in dataSources)
                {
                    var credentialInfo = new DataSourceCredentials();
                    credentialInfo.Name = dataSource.Name;
                    credentialInfo.UserId = ConfigurationManager.AppSettings["db-user"];
                    credentialInfo.Password = ConfigurationManager.AppSettings["db-password"];
                    info.Add(credentialInfo);
                }

                this.viewer.SetDataSourceCredentials(info);
            };
        }

        #endregion
    }
}
