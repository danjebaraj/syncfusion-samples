﻿namespace Syncfusion.Samples.ViewModel
{
    public class ReportModel
    {
        public string ReportPath { get; set; }
        public string ReportServerUrl { get; set; }
    }
}
